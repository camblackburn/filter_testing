# COVID-19 Filter Testing

## testing instrument
This filter efficency testing device is a blend of ASTM standards [F2299](https://www.astm.org/Standards/F2299.htm), [F2101](https://www.astm.org/Standards/F2101.htm), and the henderson apparatus detailed by Wilkes[^filterPerformance]. 

<img src="./filter_test_labeled.jpg" height="500">

Compressed air from the lab runs through a regulator, (a), then passes through the inlet HEPA filter, (b), and splits into a control valve for the nebulizer and compressed air line. The nebulizer line (d) then goes through its flowmeter, (g), and into the nebulizer, (e), where a solution with 20 - 100 nm gold particles in water is aerosolized and flows into the mixing chamber (acrylic box). The air line (c), enters the mixing chamber directly and is used to control the flow through the filter tube. A thermometer and hygrometer, (f), measures the temperature and humidity in the mixing chamber. The gas passes through a final flowmeter, (h), to control the velocity accross the filter material before entering the larger fitler tube. The filter material is cut into a 2.5 inch circle and held between removeable aluminum plates at (i). A differential pressure gauge, (j), measures the pressure drop from 2in before the filter material and 2in after. The aersoloized air travels 24 inches from the start fo the 2in tube before it hits the filter material to allow for a stabalized laminar flow. Once passing through the material, the nanoparticles are collected in an impinger, (k), and the exhaust is fed through a final HEPA filter (l). 


<img src="./filter_testing.jpg" height="500">

the entire appartus is kept in a fume hood to protect against inhalation of any stray nanoparticles - for the most aprt we are testing with gold particles which are large enough to not be of huge safety concern, but a lot of safety risks around nanoparticles is still unknown. 

<img src="./cad_1.png" height="500">

compressed air enters at the center of the device and first passes through a HEPA filter (a). 

Flowmeters read 0.4 to 4 scfm (standard cubic feet per minute) - 11.3 to 113 lpm. nebulizer needs 5 to 9 LPM (liters per minute). 1 cfm = 28.3 lpm



## quick lit review

### SARS-CoV-2 characteristics
- single stranded RNA virus, beta-coronavirus 
- "Therefore, virus transmission via respiratory secretions in the form of droplets (>5 microns) or aerosols (<5 microns) appears to be likely. " [^stability]
- nonsocomial spread of SARS-CoV-1 was linked to aerosol-generating medical procedures [^stability]
- "The virus aerosol deposition on protective apparel or floor surface and their subsequent resuspension is a potential transmission pathway and effective sanitization is critical in minimizing aerosol transmission of SARS-CoV-2." [^aerodynamics]
- airborne SARS-CoV-2 found at 0.25-0.5um and <2.5um, but not coexisting so potentially different formation mechanisms [^aerodynamics]
    - air samples in various locations collected and filtered through [cascade impactor](https://www.skcinc.com/catalog/index.php?cPath=300000000_302000000_302400000_302400200) then tested for presence of SARS-CoV-2 
    - ![](sars-CoV-2_aerodynamic_diameter.jpg) [^aerodynamics]
- "We believe one direct source of the high SARS-CoV-2 aerosol concentration may be the resuspension of virus-laden aerosol from the surface of medical staff protective apparel while they are being removed. . . We hypothesize the submicron aerosol may come from the resuspension of virus-laden aerosol from staff apparel due to its higher mobility while the supermicron virus-laden aerosol may come from the resuspension of dust particles from the floors or other hard surfaces" [^aerodynamics]

so we're targeting filter material able to block particles with minimum ~0.25um diameter and high flow rate for comfortable breathing

### mask characterization metrics
- filtration efficiency: percent efficiency of upstream colony-forming units (cfu) to downstream cfu [^DIYmask]
- fit factor: ratio of concentration of microscopic particles outside to particles that leaked in [^DIYmask]
- microbial penetration value: number of microbes passing through the filter per 10 million microbes in the challenge [^filterPerformance]
- measuring virus quantity:
    - plaque assay: plaque forming units (pfu) - virus infected cell will lyse and infect other cells, then form the plaque which can be counted manually visually or by optical microscope, takes days for virus to infect (time depending on virus)
    -   focus forming assay (FFA) - similar to pfu/mL but instead of waiting for cell to lyse add reagents to highlight infected cells, less time but more costly with equipment
    -   fifty-percent tissue culture infective dose (TCID_50) - amount of virus required to kill 50% of infected hosts. 0.69 pfu = 1 TCID bc poisson distribution 
- measuring aerosol particles:
    - MMAD: mean mass aerodynamic diameter
    - CMAD: count median aerodynamic diameter


## Filtration efficiency tests 
### biological challenge particles
- Henderson apparatus: generates closed circuit microbial aerosols from a Collison nebulizer at a controlled humidity 
   - Collison nebulizer - generally a pneumatic atomizer [^collisonNebulizer]
   - ![](henderson_apparatus.png)
- mobile henderson appartus 1968 [^henderson]
- challenge particles
    -   Bacillus atrophaeus : 0.95-1.25um, 10^7 cfu in challenge [^DIYmask]
    -   Bacteriophage MS2 MCIMB10108: single strange RNA ~20nm diameter, 10^9 pfu in challenge [^DIYmask]
    -   E. Coli phi174 [^VFEreport]
- Viable particle cascade impactor (andersen sampler) for collecting particles, incubating them for a day or two, then counting bacteria or phage growth 

performance difference between pleated hydrophobic filters and electrostatic filters??

### non-biological challenge particles 
- send colliodal spheres through atomizer (what's the diff between nebulizer and atomizer?) and use optical particel counter (OPC) to determine filtration efficiency
- ASTM standards uses latex spheres. could potentially use gold particles 
- charge neutralizer recommended - but Lorena said viruses are typically polarized so maybe charge neutralization could be ignored initially 

- optical particle counter [^opc]- fancy once can be used to measure particle size with lorentz mie scattering, but it shouldn't take too much work to make our own that just counts. 
    -   visible laser : wavelength and power determine particle size resolution, higher power --> smaller particles, limited to >100nnm (intracavity systems for smaller than that)
    -   photodetector placed where particles scatter light : placement could be difficult if the particles have a large area to explore, but photomulitplier tube could fix
    - isokinetic sampling probes[^isokineticProbes] pull representative particle samples into the OPCs - so you don't care about area for PMT and can put the OPCs in a light box instead of the entire appartus 

## proposed testing device
measure filtration efficiency and pressure difference with non-biological colloidal particles:
![](proposed_apparatus.jpg)

could also get away with only one OPC by adding a bypass around the test filter and a switch

Ah - and add in a flowmeter after the second OPC sampler!


## equipment needed
- nebulizer for aerosolization [^nebulizer]
    -   three-jet Collison nebulizer? 
- wet and dry thermometer / hygrometer for humidity 
- OPC : photodiodes or PMTs 
- andersen sampler?? could be nice to bin different size particles - six-stage viable particle cascade impactor
- OPC would be better than andersen sampler


# particle counting
### optical particle counter 

### fluorescent particle counter
fluorescently labeled microscpheres - https://www.thermofisher.com/order/catalog/product/93470350011150?SID=srch-srp-93470350011150#/93470350011150?SID=srch-srp-93470350011150 \
Measure particles: \
CBA has a plate reader - can return flourescent concentration (not individual particle #) but requries optimization \
or confocal microscope - collect particels in iminger with water solution, let colloidal solution evaporate leaving particles on stage and record particle desnity in an image


### microgram mass comparison of gold particles 
test measurement with 100nm gold particles \
from bbi solutions dataseet for 100nm gold particles - mass of 1L of gold is 5.66E-05 g, too small for the analytical balance in BL1 \
also, I tried measuring evaporated solution on a few microscope slides and the slides are not uniform enough to do comparative measurements. \
but can see the particles in the Olympus microscope 
![](100nm_gold_particles.jpg)

### spectrophotometer
Implen NP80 nanophotometer picks up 100 nm Au particles! simplest relative measuring technique 

## common units & conversions
SCFM: standard cubic feet per minute - defined for a set pressure and temperature, not the actual cubic feet per minute (need to find conversion factor for flowmeter reading). molar flow rate = molar constant * mass flowrate

LPM: liters per minute, volumetric flow rate

(s)ccm: (standard) cubic centimeter per minute, mass flow rate (even though it's cubis centimeter) so must take into account fluid/gas density 

conversions:
- 1 scfm = 28,317 sccm
 


### Resources
[^DIYmask]: University of Cambridge [DIY mask efficacy testing](https://www.researchgate.net/publication/258525804_Testing_the_Efficacy_of_Homemade_Masks_Would_They_Protect_in_an_Influenza_Pandemic)
[^filterPerformance]: AR Wilkes [The bacterial and viral filtration performance of breathing system filters](https://onlinelibrary.wiley.com/doi/full/10.1046/j.1365-2044.2000.01327.x)
[^henderson]: [Mobile Henderson apparatus](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2130736/pdf/jhyg00097-0062.pdf)
[^collisonNebulizer]: [Collison Nebulizer](https://arxiv.org/pdf/2001.04498.pdf)
[^stability]: [Aeorsol and surface stability of HCoV-19 compared to SARS-CoV-1](https://www.medrxiv.org/content/10.1101/2020.03.09.20033217v1.full.pdf?__cf_chl_jschl_tk__=e5c36168cc7402ab11178538c0ef5fdd2b8c8cbd-1584464414-0-AfohFaEsIDDjmqQ6YEGAmPrX14_fYwDCairndak5B58MzTDypSP7pDWh1jiStM9Xr_70C8k2y_R4fLiH9CFS5W4Y8Q3u3jMABGFCptEt34BXergTOg1G120143eK6Wib1g34BQflq9yKQ5kuuKjU7or6pxdvmtOIsI45onaedQTa8Ioxpo-tMjo83zh6akUTE8BynDs6RQSLB-Ey8zsmuVgG4KLh38sb-sNoxZGizcaBZ7QmCUQVEZlzBKlbKMpAqBCvSOizA0FD2LgWVBFa1UlJ1DYF-l0_-hC0xFvK3lsbZHriRizQA_KZqylQ9l9QFMQo3642MuAGGRCbzwkcC_0)
[^aerodynamics]: [Aerodynamic Characteristics of RNA concetration of SARS-CoV-2](https://www.biorxiv.org/content/10.1101/2020.03.08.982637v1.abstract)
[^VFEreport]: [Nelson Laboratories: Viral Filtration Efficiency (VFE) at increased challenge level](https://gitlab.cba.mit.edu/pub/coronavirus/tracking/uploads/164d119b5a7a5751c685a6677378668a/ER_2235600_Attachment_5_Viral_Filtration_Efficiency.pdf)
[^nelson]: [Nelson lab filtration test](https://www.nelsonlabs.com/testing/bacterial-viral-filtration-efficiency-bfe-vfe/?industry=medical-devices&category=protective-barriers-material-performance)
[^nebulizer]: [Nebulizer selection from CH tech](http://chtechusa.com/Manuals/Selecting%20the%20Model%20and%20Configuration%20Instructions.pdf)
[^opc]: [optical particle counter](http://www.cas.manchester.ac.uk/restools/instruments/aerosol/opc/)
[^isokineticProbes]: [Isokinetic particle Sampling](http://www.climet.com/accessories/isoprobes.html)